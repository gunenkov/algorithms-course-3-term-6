﻿using System;
using TreeAlgorithms.Binary;
using TreeAlgorithms.Models;

namespace Lab2
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //Task1();
            Task2();
        }

        public static void Task1()
        {
            // LR 2 Task 1
            var tree = new BinarySearchTree<int, int>();
            int currentNumber;
            while ((currentNumber = Convert.ToInt32(Console.ReadLine())) != 0)
            {
                var node = tree.GetByKey(currentNumber);
                if (node == null)
                    tree.Insert(currentNumber, 1);
                else
                    tree.UpdateOrInsert(currentNumber, node.Value + 1);
            }

            foreach (var node in tree.InOrderTraversal()) Console.Write($"Key: {node.Key}\tValue: {node.Value}\n");
        }

        public static void Task2()
        {
            // LR 2 Task 2

            // 1 операция
            var tree = new BinarySearchTree<long, PhoneInfo>();

            // 6 раз по ... операций
            tree.Insert(643464, new PhoneInfo {SubscriberInfo = "Михаил Гуненков", TariffId = 0});
            tree.Insert(958673, new PhoneInfo {SubscriberInfo = "Артем", TariffId = 2});
            tree.Insert(245346, new PhoneInfo {SubscriberInfo = "Елена", TariffId = 2});
            tree.Insert(895609, new PhoneInfo {SubscriberInfo = "Катя", TariffId = 2});
            tree.Insert(346274, new PhoneInfo {SubscriberInfo = "Артемий", TariffId = 3});
            tree.Insert(266848, new PhoneInfo {SubscriberInfo = "Анатолий", TariffId = 1});

            // 2 * log n + 2
            foreach (var node in tree.InOrderTraversal()) Console.Write($"Key: {node.Key}\tValue: {node.Value}\n");

            // 3*log n + 2 операций
            Console.WriteLine($"Владельцем номера 895609 является {tree.GetByKey(895609).Value}");

            Console.WriteLine($"Наиболее востребованный тариф: {tree.GetMostPopularTariff()}");
        }
    }
}