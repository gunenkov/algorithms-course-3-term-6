﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab7
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var items = new List<Item>
            {
                new Item("Книга", 1, 600),
                new Item("Бинокль", 2, 5000),
                new Item("Аптечка", 4, 1500),
                new Item("Ноутбук", 2, 40000),
                new Item("Котелок", 1, 500)
            };

            var backpackWeightLimit = 8.0;

            foreach (var item in BruteForce(items, backpackWeightLimit)) Console.WriteLine(item);
        }

        private static IEnumerable<Item> BruteForce(IList<Item> items, double backpackWeightLimit)
        {
            var binaryVectors = new List<int[]>();
            var initialVector = Enumerable.Repeat(0, items.Count).ToArray();
            binaryVectors.Add((int[]) initialVector.Clone());
            for (var i = 0; i < Math.Pow(2, items.Count); i++)
            {
                var vector = (int[]) binaryVectors.Last().Clone();
                for (var j = items.Count - 1; j >= 0; j--)
                {
                    if (vector[j] != 0) continue;

                    vector[j] = 1;
                    for (var k = j + 1; k < items.Count; k++) vector[k] = 0;
                    binaryVectors.Add((int[]) vector.Clone());
                }
            }

            var costsWithVectors = new List<Tuple<double, int[]>>();

            foreach (var vector in binaryVectors)
            {
                var cost = 0.0;
                var weight = 0.0;
                for (var j = 0; j < vector.Length; j++)
                {
                    cost += items[j].Cost * vector[j];
                    weight += items[j].Weight * vector[j];
                }

                if (weight < backpackWeightLimit) costsWithVectors.Add(Tuple.Create(cost, (int[]) vector.Clone()));
            }

            var optimalVector = costsWithVectors.OrderByDescending(tuple => tuple.Item1).First().Item2;


            var itemsInBackpack = new List<Item>();
            for (var i = 0; i < optimalVector.Length; i++)
                if (optimalVector[i] == 1)
                    itemsInBackpack.Add(items[i]);

            return itemsInBackpack;
        }

        private static IEnumerable<Item> Greedy(IList<Item> items, double backpackWeightLimit)
        {
            foreach (var item in items) item.C = item.Cost / item.Weight;

            items = items.OrderByDescending(item => item.C).ToList();

            var itemsInBackpack = new List<Item>();

            var sum = 0.0;
            foreach (var item in items)
            {
                sum += item.Weight;
                if (sum > backpackWeightLimit) break;
                itemsInBackpack.Add(item);
            }

            return itemsInBackpack;
        }

        public class Item
        {
            public Item(string name, double weight, double cost)
            {
                Name = name;
                Weight = weight;
                Cost = cost;
            }

            public string Name { get; set; }
            public double Weight { get; set; }
            public double Cost { get; set; }
            public double C { get; set; }

            public override string ToString()
            {
                return Name;
            }
        }
    }
}