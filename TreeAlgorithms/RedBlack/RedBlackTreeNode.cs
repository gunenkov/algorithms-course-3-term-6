﻿using System;

namespace TreeAlgorithms.RedBlack
{
    public class RedBlackTreeNode<TK, TV> where TK : IComparable
    {
        public Color Color;

        public TK Key;
        public RedBlackTreeNode<TK, TV> LeftNode;
        public RedBlackTreeNode<TK, TV> Parent;
        public RedBlackTreeNode<TK, TV> RightNode;
        public TV Value;

        public RedBlackTreeNode(TK key, TV value)
        {
            Key = key;
            Value = value;
        }

        public RedBlackTreeNode(TK key)
        {
            Key = key;
        }

        public RedBlackTreeNode(Color color)
        {
            Color = color;
        }

        public RedBlackTreeNode(TK key, TV value, Color color)
        {
            Key = key;
            Value = value;
            Color = color;
        }
    }
}