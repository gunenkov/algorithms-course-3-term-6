﻿using System;
using System.Collections.Generic;

namespace TreeAlgorithms.RedBlack
{
    public class RedBlackTree<TK, TV> where TK : IComparable
    {
        public RedBlackTreeNode<TK, TV> Root;

        public IList<RedBlackTreeNode<TK, TV>> InOrderTraversalByLevels()
        {
            var nodes = new List<RedBlackTreeNode<TK, TV>>();
            for (var i = 1; i <= GetSubtreeHeight(Root); i++) InOrderTraversalCurrentLevel(Root, i, nodes);
            return nodes;
        }

        private static int GetSubtreeHeight(RedBlackTreeNode<TK, TV> currentNode)
        {
            if (currentNode == null) return 0;
            return Math.Max(GetSubtreeHeight(currentNode.LeftNode), GetSubtreeHeight(currentNode.RightNode)) + 1;
        }

        private static void InOrderTraversalCurrentLevel(RedBlackTreeNode<TK, TV> root, int level,
            ICollection<RedBlackTreeNode<TK, TV>> nodes)
        {
            if (root == null) return;
            if (level == 1)
            {
                nodes.Add(root);
            }
            else if (level > 1)
            {
                InOrderTraversalCurrentLevel(root.LeftNode, level - 1, nodes);
                InOrderTraversalCurrentLevel(root.RightNode, level - 1, nodes);
            }
        }

        private static void LeftRotate(RedBlackTreeNode<TK, TV> root)
        {
            var pivot = root.RightNode;
            root.RightNode = pivot.LeftNode;
            if (pivot.LeftNode != null) pivot.LeftNode.Parent = root;
            if (pivot != null) pivot.Parent = root.Parent;
            if (root.Parent == null) root = pivot;
            if (root == root.Parent.LeftNode)
                root.Parent.LeftNode = pivot;
            else
                root.Parent.RightNode = pivot;
            pivot.LeftNode = root;
            if (root != null) root.Parent = pivot;
        }

        private void RightRotate(RedBlackTreeNode<TK, TV> root)
        {
            var pivot = root.LeftNode;
            root.LeftNode = pivot.RightNode;
            if (pivot.RightNode != null) pivot.RightNode.Parent = root;
            if (pivot != null) pivot.Parent = root.Parent;
            if (root.Parent == null) root = pivot;
            if (root == root.Parent.RightNode) root.Parent.RightNode = pivot;
            if (root == root.Parent.LeftNode) root.Parent.LeftNode = pivot;

            pivot.RightNode = root;
            if (root != null) root.Parent = pivot;
        }

        public void Insert(TK key, TV value)
        {
            var itemToInsert = new RedBlackTreeNode<TK, TV>(key, value);
            if (Root == null)
            {
                Root = itemToInsert;
                Root.Color = Color.Black;
                return;
            }

            RedBlackTreeNode<TK, TV> parent = null;
            var currentNodeToCompareWith = Root;
            while (currentNodeToCompareWith != null)
            {
                parent = currentNodeToCompareWith;
                if (itemToInsert.Key.CompareTo(currentNodeToCompareWith.Key) < 0)
                    currentNodeToCompareWith = currentNodeToCompareWith.LeftNode;
                else
                    currentNodeToCompareWith = currentNodeToCompareWith.RightNode;
            }

            itemToInsert.Parent = parent;
            if (parent == null)
                Root = itemToInsert;
            else if (itemToInsert.Key.CompareTo(parent.Key) < 0)
                parent.LeftNode = itemToInsert;
            else
                parent.RightNode = itemToInsert;
            itemToInsert.LeftNode = null;
            itemToInsert.RightNode = null;
            itemToInsert.Color = Color.Red;
            BalanceAfterInsertion(itemToInsert);
        }

        private void BalanceAfterInsertion(RedBlackTreeNode<TK, TV> currentNode)
        {
            while (currentNode != Root && currentNode.Parent.Color == Color.Red)
            {
                // если родитель текущей ноды - левый потомок своего родителя
                if (currentNode.Parent == currentNode.Parent.Parent.LeftNode)
                {
                    // находим дядю
                    var uncle = currentNode.Parent.Parent.RightNode;
                    // если дядя красный
                    if (uncle != null && uncle.Color == Color.Red)
                    {
                        currentNode.Parent.Color = Color.Black;
                        uncle.Color = Color.Black;
                        currentNode.Parent.Parent.Color = Color.Red;
                        currentNode = currentNode.Parent.Parent;
                    }
                    // если дядя черный
                    else
                    {
                        if (currentNode == currentNode.Parent.RightNode)
                        {
                            currentNode = currentNode.Parent;
                            LeftRotate(currentNode);
                        }

                        currentNode.Parent.Color = Color.Black;
                        currentNode.Parent.Parent.Color = Color.Red;
                        RightRotate(currentNode.Parent.Parent);
                    }
                }

                // по аналогии
                else
                {
                    var uncle = currentNode.Parent.Parent.LeftNode;
                    if (uncle != null && uncle.Color == Color.Black)
                    {
                        currentNode.Parent.Color = Color.Red;
                        uncle.Color = Color.Red;
                        currentNode.Parent.Parent.Color = Color.Black;
                        currentNode = currentNode.Parent.Parent;
                    }
                    else
                    {
                        if (currentNode == currentNode.Parent.LeftNode)
                        {
                            currentNode = currentNode.Parent;
                            RightRotate(currentNode);
                        }

                        currentNode.Parent.Color = Color.Black;
                        currentNode.Parent.Parent.Color = Color.Red;
                        LeftRotate(currentNode.Parent.Parent);
                    }
                }

                Root.Color = Color.Black;
            }
        }

        public RedBlackTreeNode<TK, TV> Find(TK key)
        {
            var currentNode = Root;
            while (true)
            {
                if (currentNode == null) break;
                if (key.CompareTo(currentNode.Key) < 0)
                    currentNode = currentNode.LeftNode;
                else if (key.CompareTo(currentNode.Key) > 0)
                    currentNode = currentNode.RightNode;
                else
                    return currentNode;
            }

            throw new Exception("Нода не найдена");
        }

        private static RedBlackTreeNode<TK, TV> GetMinimum(RedBlackTreeNode<TK, TV> root)
        {
            while (root.LeftNode.LeftNode != null) root = root.LeftNode;

            if (root.LeftNode.RightNode != null) root = root.LeftNode.RightNode;

            return root;
        }

        private static RedBlackTreeNode<TK, TV> GetSuccessor(RedBlackTreeNode<TK, TV> currentNode)
        {
            if (currentNode.LeftNode != null) return GetMinimum(currentNode);

            var parent = currentNode.Parent;
            while (parent != null && currentNode == parent.RightNode)
            {
                currentNode = parent;
                parent = parent.Parent;
            }

            return parent;
        }

        public void Delete(TK key)
        {
            var item = Find(key);
            RedBlackTreeNode<TK, TV> successorDescendant;
            RedBlackTreeNode<TK, TV> successor;

            if (item.LeftNode == null || item.RightNode == null)
                successor = item;
            else
                successor = GetSuccessor(item);

            if (successor.LeftNode != null)
                successorDescendant = successor.LeftNode;
            else
                successorDescendant = successor.RightNode;

            if (successorDescendant != null) successorDescendant.Parent = successor;

            if (successor.Parent == null)
                Root = successorDescendant;
            else if (successor == successor.Parent.LeftNode)
                successor.Parent.LeftNode = successorDescendant;
            else
                successor.Parent.LeftNode = successorDescendant;

            if (successor != item) item.Key = successor.Key;

            if (successor.Color == Color.Black) BalanceAfterDelete(successorDescendant);
        }

        private void BalanceAfterDelete(RedBlackTreeNode<TK, TV> currentNode)
        {
            while (currentNode != null && currentNode != Root && currentNode.Color == Color.Black)
                if (currentNode == currentNode.Parent.LeftNode)
                {
                    var brother = currentNode.Parent.RightNode;
                    if (brother.Color == Color.Red)
                    {
                        brother.Color = Color.Black;
                        currentNode.Parent.Color = Color.Red;
                        LeftRotate(currentNode.Parent);
                        brother = currentNode.Parent.RightNode;
                    }

                    if (brother.LeftNode.Color == Color.Black && brother.RightNode.Color == Color.Black)
                    {
                        brother.Color = Color.Red;
                        currentNode = currentNode.Parent;
                    }
                    else if (brother.RightNode.Color == Color.Black)
                    {
                        brother.LeftNode.Color = Color.Black;
                        brother.Color = Color.Red;
                        RightRotate(brother);
                        brother = currentNode.Parent.RightNode;
                    }

                    brother.Color = currentNode.Parent.Color;
                    currentNode.Parent.Color = Color.Black;
                    brother.RightNode.Color = Color.Black;
                    LeftRotate(currentNode.Parent);
                    currentNode = Root;
                }
                else
                {
                    var brother = currentNode.Parent.LeftNode;
                    if (brother.Color == Color.Red)
                    {
                        brother.Color = Color.Black;
                        currentNode.Parent.Color = Color.Red;
                        RightRotate(currentNode.Parent);
                        brother = currentNode.Parent.LeftNode;
                    }

                    if (brother.RightNode.Color == Color.Black && brother.LeftNode.Color == Color.Black)
                    {
                        brother.Color = Color.Black;
                        currentNode = currentNode.Parent;
                    }
                    else if (brother.LeftNode.Color == Color.Black)
                    {
                        brother.RightNode.Color = Color.Black;
                        brother.Color = Color.Red;
                        LeftRotate(brother);
                        brother = currentNode.Parent.LeftNode;
                    }

                    brother.Color = currentNode.Parent.Color;
                    currentNode.Parent.Color = Color.Black;
                    brother.LeftNode.Color = Color.Black;
                    RightRotate(currentNode.Parent);
                    currentNode = Root;
                }

            if (currentNode != null)
                currentNode.Color = Color.Black;
        }
    }
}