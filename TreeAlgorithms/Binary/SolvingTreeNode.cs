﻿using System;
using TreeAlgorithms.Helpers;

namespace TreeAlgorithms.Binary
{
    public class SolvingTreeNode
    {
        public SolvingTreeNode LeftNode;
        public SolvingTreeNode RightNode;

        public char Value;

        public SolvingTreeNode(char value)
        {
            Value = value;
        }

        public void InsertFormulaTokensRecursive(string leftSubstring, string rightSubstring)
        {
            if (leftSubstring.Length > 1)
            {
                if (leftSubstring[0] == '(' && leftSubstring[^1] == ')')
                    leftSubstring = leftSubstring.Substring(1, leftSubstring.Length - 2);

                var brackets = false;
                var indexOfLowestPriorityOperator = leftSubstring.Length - 1;
                var lowestOperationPriority = 2;
                for (var i = 0; i < leftSubstring.Length; i++)
                {
                    if (leftSubstring[i] == '(') brackets = true;

                    if (leftSubstring[i] == ')') brackets = false;

                    if (!SolvingTreeHelper.Operators.Contains(leftSubstring[i]) || brackets) continue;

                    var currentOperatorPriority = SolvingTreeHelper.GetOperatorPriority(leftSubstring[i]);
                    if (currentOperatorPriority < lowestOperationPriority)
                    {
                        indexOfLowestPriorityOperator = i;
                        lowestOperationPriority = currentOperatorPriority;
                    }
                }

                LeftNode = new SolvingTreeNode(leftSubstring[indexOfLowestPriorityOperator]);
                LeftNode.InsertFormulaTokensRecursive(leftSubstring.Substring(0,
                        indexOfLowestPriorityOperator),
                    leftSubstring.Substring(indexOfLowestPriorityOperator + 1));
            }

            if (leftSubstring.Length == 1) LeftNode = new SolvingTreeNode(leftSubstring[0]);

            if (rightSubstring.Length > 1)
            {
                if (rightSubstring[0] == '(' && rightSubstring[^1] == ')')
                    rightSubstring = rightSubstring.Substring(1, rightSubstring.Length - 2);

                var brackets = false;
                var indexOfLowestPriorityOperator = rightSubstring.Length - 1;
                var lowestOperationPriority = 2;

                for (var i = 0; i < rightSubstring.Length; i++)
                {
                    if (rightSubstring[i] == '(') brackets = true;

                    if (rightSubstring[i] == ')') brackets = false;

                    if (!SolvingTreeHelper.Operators.Contains(rightSubstring[i]) || brackets) continue;

                    var currentOperatorPriority = SolvingTreeHelper.GetOperatorPriority(rightSubstring[i]);
                    if (currentOperatorPriority < lowestOperationPriority)
                    {
                        indexOfLowestPriorityOperator = i;
                        lowestOperationPriority = currentOperatorPriority;
                    }
                }

                RightNode = new SolvingTreeNode(rightSubstring[indexOfLowestPriorityOperator]);
                RightNode.InsertFormulaTokensRecursive(rightSubstring.Substring(0,
                        indexOfLowestPriorityOperator),
                    rightSubstring.Substring(indexOfLowestPriorityOperator + 1));
            }

            else if (rightSubstring.Length == 1)
            {
                RightNode = new SolvingTreeNode(rightSubstring[0]);
            }
        }

        public void PostOrderTraversal()
        {
            LeftNode?.PostOrderTraversal();
            RightNode?.PostOrderTraversal();
            Console.WriteLine(Value);
        }
    }
}