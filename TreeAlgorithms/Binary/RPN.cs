﻿using TreeAlgorithms.Helpers;

namespace TreeAlgorithms.Binary
{
    public class RPN
    {
        public static void ConvertFormulaToRpn(string formula)
        {
            var tree = new SolvingTree();

            var brackets = false;
            var indexOfLowestPriorityOperator = formula.Length - 1;
            var lowestOperationPriority = 2;

            for (var i = 0; i < formula.Length; i++)
            {
                if (formula[i] == '(') brackets = true;

                if (formula[i] == ')') brackets = false;

                if (!SolvingTreeHelper.Operators.Contains(formula[i]) || brackets) continue;
                var currentOperatorPriority = SolvingTreeHelper.GetOperatorPriority(formula[i]);
                if (currentOperatorPriority <= lowestOperationPriority)
                {
                    indexOfLowestPriorityOperator = i;
                    lowestOperationPriority = currentOperatorPriority;
                }
            }

            tree.Root = new SolvingTreeNode(formula[indexOfLowestPriorityOperator]);
            tree.Root.InsertFormulaTokensRecursive(formula.Substring(0,
                indexOfLowestPriorityOperator), formula.Substring(indexOfLowestPriorityOperator + 1));

            tree.PostOrderTraversal();
        }
    }
}