﻿namespace TreeAlgorithms.Binary
{
    public class SolvingTree
    {
        public SolvingTreeNode Root;

        public void PostOrderTraversal()
        {
            Root?.PostOrderTraversal();
        }
    }
}