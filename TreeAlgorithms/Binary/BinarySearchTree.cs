﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TreeAlgorithms.Binary
{
    public class BinarySearchTree<TK, TV> where TK : IComparable
    {
        private readonly IList<BinarySearchTreeNode<TK, TV>> _nodes = new List<BinarySearchTreeNode<TK, TV>>();
        private readonly Dictionary<int, int> _tariffCounts = new Dictionary<int, int>();
        public BinarySearchTreeNode<TK, TV> Root { get; private set; }

        public void Insert(TK key, TV value)
        {
            // если дерево уже сформировано (корень есть)
            // 1 операция
            if (Root != null)
                // вызываем рекурсивную процедуру вставки ноды относительно текущей ноды (изначально - корня)
                Root.Insert(key, value);
            // если корня нет - создаем
            else
                Root = new BinarySearchTreeNode<TK, TV>(key, value);
        }

        public void Remove(int key)
        {
            var current = Root;
            var parent = Root;
            // если true - текущая вершина (current) - левый потомок родителя (parent)
            var isLeftChild = false;

            // дерево не инициализировано (корня нет) - удалять нечего
            if (current == null) throw new Exception("Дерево пустое. Удалять нечего");

            // ищем вершину, которую будем удалять
            while (current != null && !current.Key.Equals(key))
            {
                parent = current;

                if (key.CompareTo(current.Key) < 0)
                {
                    current = current.LeftNode;
                    isLeftChild = true;
                }
                else
                {
                    current = current.RightNode;
                    isLeftChild = false;
                }
            }

            // если не нашли ноду с нужным Id - удалять нечего
            if (current == null) throw new Exception("Запись с таким Id не найдена. Удалять нечего");

            // анализируем тип ноды, которую нашли
            // если это лист (оба потомка равны null)
            if (current.RightNode == null && current.LeftNode == null)
            {
                if (current == Root)
                {
                    Root = null;
                }
                else
                {
                    // если найденная вершина - левый потомок своего родителя
                    if (isLeftChild)
                        // удаляем ссылку у родителя на левый потомок
                        parent.LeftNode = null;
                    else
                        // иначе удаляем ссылку у родителя на правый потомок
                        parent.RightNode = null;
                }
            }
            // если удаляемая нода имеет только левого потомка
            else if (current.RightNode == null)
            {
                if (current == Root)
                {
                    Root = current.LeftNode;
                }
                else
                {
                    // если удаляемая нода является левым потомков своего родителя
                    if (isLeftChild)
                        // левым потомком родителя становится левый потомок удаляемой ноды
                        parent.LeftNode = current.LeftNode;
                    // если удаляемая нода является правым потомков своего родителя
                    else
                        // правым потомком родителя является левый потомок удаляемой ноды
                        parent.RightNode = current.LeftNode;
                }
            }
            // действуем по аналогии со случаем, когда текущая вершина имеет только левого потомка
            else if (current.LeftNode == null)
            {
                if (current == Root)
                {
                    Root = current.RightNode;
                }
                else
                {
                    // если удаляемая нода является левым потомков своего родителя
                    if (isLeftChild)
                        parent.LeftNode = current.RightNode;
                    // если удаляемая нода является правым потомков своего родителя
                    else
                        parent.RightNode = current.RightNode;
                }
            }
            // удаляемая нода имеет левого и правого потомков
            else
            {
                // находим ноду, Id которой больше, чем Id текущей, но меньше, чем Id остальных нод, 
                // Id которых больше чем текущей - назовем ее "последователем" ("successor")

                var successor = GetSuccessor(current);

                // если удаляемая нода - корень, значит, что корнем сделаем ее последователя
                if (current == Root)
                    Root = successor;
                // если текущая нода является левым потомком своего родителя
                else if (isLeftChild)
                    // тогда левым потомком родителя удаляемой ноды делаем ее последователя
                    parent.LeftNode = successor;
                // если текущая нода является правым потомком своего родителя
                else
                    // тогда правым потомком родителя удаляемой ноды делаем ее последователя
                    parent.RightNode = successor;
            }
        }

        private BinarySearchTreeNode<TK, TV> GetSuccessor(BinarySearchTreeNode<TK, TV> node)
        {
            var parentOfSuccessor = node;
            var successor = node;
            var current = node.RightNode;

            // начиная от правого потомка удаляемой ноды начинаем двигаться исключительно влево, до листа
            while (current != null)
            {
                parentOfSuccessor = successor;
                successor = current;
                current = current.LeftNode;
            }

            /*
             * возможна ситуация, когда последователь удаляемой ноды - это ее правый потомок;
             * в этом случае, при замене удаляемой ноды ее последователем, структура связей от
             * правого потомка последователя сохранится
             * рассмотрим вариант, если последователь НЕ является правым потомком удаляемой ноды
             */
            if (successor != node.RightNode)
            {
                // в качестве левого потомка родителя последователя поставим правого потомка последователя
                // после этой операции связь между последователем и родителем разрывается!
                parentOfSuccessor.LeftNode = successor.RightNode;
                // в качестве правого потомка последователя устанавливаем правого потомка удаляемой ноды
                successor.RightNode = node.RightNode;
            }

            /*
             * привязываем в качестве левого потомка последователя левого потомка удаляемой ноды, чтобы сохранить
             * связи, идущие от левого потомка удаляемой ноды после замены удаляемой ноды ее последователем;
             * теперь на левого потомка удаляемой ноды указывают две ссылки - с удаляемой ноды и ее последователя
             */
            successor.LeftNode = node.LeftNode;

            return successor;
        }

        public BinarySearchTreeNode<TK, TV> GetByKey(TK key)
        {
            // 1 операция
            return Root?.GetByKey(key);
        }

        public void UpdateOrInsert(TK key, TV value)
        {
            var node = Root?.GetByKey(key);
            if (node != null)
            {
                node.Value = value;
                return;
            }

            Insert(key, value);
        }

        public IList<BinarySearchTreeNode<TK, TV>> InOrderTraversal()
        {
            // 1 операция
            _nodes.Clear();

            Root?.InOrderTraversal(_nodes);
            // 1 операция
            return new List<BinarySearchTreeNode<TK, TV>>(_nodes);
        }

        public int GetMostPopularTariff()
        {
            _nodes.Clear();

            Root?.GetMostPopularTariff(_tariffCounts);
            // 1 операция
            return _tariffCounts.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value).Last().Key;
        }

        public IList<BinarySearchTreeNode<TK, TV>> PostOrderTraversal()
        {
            _nodes.Clear();
            Root?.PostOrderTraversal(_nodes);
            return new List<BinarySearchTreeNode<TK, TV>>(_nodes);
        }
    }
}