﻿using System;
using System.Collections.Generic;
using TreeAlgorithms.Models;

namespace TreeAlgorithms.Binary
{
    public class BinarySearchTreeNode<TK, TV> where TK : IComparable
    {
        public BinarySearchTreeNode<TK, TV> LeftNode;

        public BinarySearchTreeNode<TK, TV> RightNode;

        public BinarySearchTreeNode(TK key, TV value)
        {
            Key = key;
            Value = value;
        }

        public TK Key { get; }
        public TV Value { get; set; }

        public void Insert(TK key, TV value)
        {
            // если ключ вставляемой ноды БОЛЬШЕ чем ключ текущей ноды
            if (key.CompareTo(Key) > 0)
            {
                // смотрим правого потомка
                // если лист
                if (RightNode == null)
                    RightNode = new BinarySearchTreeNode<TK, TV>(key, value);
                // если не лист - продолжаем двигаться
                else
                    RightNode.Insert(key, value);
            }
            // если ключ вставляемой ноды МЕНЬШЕ чем ключ текущей ноды
            else if (key.CompareTo(Key) < 0)
            {
                // смотрим левого потомка
                // если лист
                if (LeftNode == null)
                    LeftNode = new BinarySearchTreeNode<TK, TV>(key, value);
                // если не лист - продолжаем двигаться
                else
                    LeftNode.Insert(key, value);
            }
            else
            {
                throw new Exception("Элемент с таким Id уже существует в дереве. Вставка невозможна");
            }
        }

        public BinarySearchTreeNode<TK, TV> GetByKey(TK key)
        {
            // 1 операция
            var currentNode = this;

            // log n
            while (currentNode != null)
                // 3 операции
                if (key.CompareTo(currentNode.Key) > 0)
                    currentNode = currentNode.RightNode;
                else if (key.CompareTo(currentNode.Key) < 0)
                    currentNode = currentNode.LeftNode;
                else
                    return currentNode;
            return null;
        }

        public void InOrderTraversal(IList<BinarySearchTreeNode<TK, TV>> nodes)
        {
            // log n
            LeftNode?.InOrderTraversal(nodes);
            // 1 операция
            nodes.Add(this);
            // log n
            RightNode?.InOrderTraversal(nodes);
        }

        public void GetMostPopularTariff(IDictionary<int, int> tariffCounts)
        {
            // log n
            LeftNode?.GetMostPopularTariff(tariffCounts);

            var tariffId = (Value as PhoneInfo).TariffId;

            if (tariffCounts.ContainsKey(tariffId))
                tariffCounts[tariffId]++;
            else
                tariffCounts.Add(tariffId, 1);

            // log n
            RightNode?.GetMostPopularTariff(tariffCounts);
        }

        public void PostOrderTraversal(IList<BinarySearchTreeNode<TK, TV>> nodes)
        {
            LeftNode?.PostOrderTraversal(nodes);
            RightNode?.PostOrderTraversal(nodes);
            nodes.Add(this);
        }
    }
}