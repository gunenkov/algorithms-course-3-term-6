﻿using System.Collections.Generic;

namespace TreeAlgorithms.Helpers
{
    public static class SolvingTreeHelper
    {
        public static List<char> Operators = new List<char> {'+', '-', '*', '/'};

        public static int GetOperatorPriority(char operator_)
        {
            if (operator_ == '+' || operator_ == '-') return 0;

            return 1;
        }
    }
}