﻿using System;
using System.Collections.Generic;

namespace TreeAlgorithms.Abstractions
{
    public abstract class AbstractNode<TK, TV> where TK : IComparable
    {
        public AbstractNode<TK, TV> LeftNode;

        public AbstractNode<TK, TV> RightNode;

        public AbstractNode(TK key, TV value)
        {
            Key = key;
            Value = value;
        }

        public TK Key { get; }
        public TV Value { get; set; }

        public abstract void Insert(TK key, TV value);

        public abstract AbstractNode<TK, TV> GetByKey(TK key);

        public void InOrderTraversal(IList<AbstractNode<TK, TV>> nodes)
        {
            // log n
            LeftNode?.InOrderTraversal(nodes);
            // 1 операция
            nodes.Add(this);
            // log n
            RightNode?.InOrderTraversal(nodes);
        }

        public void PostOrderTraversal(IList<AbstractNode<TK, TV>> nodes)
        {
            LeftNode?.PostOrderTraversal(nodes);
            RightNode?.PostOrderTraversal(nodes);
            nodes.Add(this);
        }
    }
}