﻿using System;
using System.Collections.Generic;

namespace TreeAlgorithms.Abstractions
{
    public abstract class AbstractTree<TK, TV> where TK : IComparable
    {
        protected readonly IList<AbstractNode<TK, TV>> _nodes = new List<AbstractNode<TK, TV>>();
        public AbstractNode<TK, TV> Root { get; set; }

        public abstract void Insert(TK key, TV value);
        public abstract void Remove(TK key);

        protected AbstractNode<TK, TV> GetSuccessor(AbstractNode<TK, TV> node)
        {
            var parentOfSuccessor = node;
            var successor = node;
            var current = node.RightNode;

            // начиная от правого потомка удаляемой ноды начинаем двигаться исключительно влево, до листа
            while (current != null)
            {
                parentOfSuccessor = successor;
                successor = current;
                current = current.LeftNode;
            }

            /*
             * возможна ситуация, когда последователь удаляемой ноды - это ее правый потомок;
             * в этом случае, при замене удаляемой ноды ее последователем, структура связей от
             * правого потомка последователя сохранится
             * рассмотрим вариант, если последователь НЕ является правым потомком удаляемой ноды
             */
            if (successor != node.RightNode)
            {
                // в качестве левого потомка родителя последователя поставим правого потомка последователя
                // после этой операции связь между последователем и родителем разрывается!
                parentOfSuccessor.LeftNode = successor.RightNode;
                // в качестве правого потомка последователя устанавливаем правого потомка удаляемой ноды
                successor.RightNode = node.RightNode;
            }

            /*
             * привязываем в качестве левого потомка последователя левого потомка удаляемой ноды, чтобы сохранить
             * связи, идущие от левого потомка удаляемой ноды после замены удаляемой ноды ее последователем;
             * теперь на левого потомка удаляемой ноды указывают две ссылки - с удаляемой ноды и ее последователя
             */
            successor.LeftNode = node.LeftNode;

            return successor;
        }

        public AbstractNode<TK, TV> GetByKey(TK key)
        {
            // 1 операция
            return Root?.GetByKey(key);
        }
    }
}