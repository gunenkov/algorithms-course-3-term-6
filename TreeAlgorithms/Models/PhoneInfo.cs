﻿namespace TreeAlgorithms.Models
{
    public class PhoneInfo
    {
        public string SubscriberInfo;
        public int TariffId;

        public override string ToString()
        {
            return $"{SubscriberInfo} - ID тарифа: {TariffId}";
        }
    }
}