﻿namespace TreeAlgorithms.Models
{
    public class Student
    {
        public string Name;

        public Student(string name)
        {
            Name = name;
        }
    }
}