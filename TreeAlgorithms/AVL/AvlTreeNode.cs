﻿using System;

namespace TreeAlgorithms.AVL
{
    public class AvlTreeNode<TK, TV> where TK : IComparable
    {
        public AvlTreeNode(TK key, TV value)
        {
            Key = key;
            Value = value;
        }

        public TK Key { get; set; }
        public TV Value { get; set; }

        public AvlTreeNode<TK, TV> RightNode { get; set; }
        public AvlTreeNode<TK, TV> LeftNode { get; set; }
    }
}