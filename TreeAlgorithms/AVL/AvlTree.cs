﻿using System;
using System.Collections.Generic;

namespace TreeAlgorithms.AVL
{
    public class AvlTree<TK, TV> where TK : IComparable
    {
        public AvlTreeNode<TK, TV> Root { get; set; }

        public void InsertOrUpdate(TK key, TV value)
        {
            var node = new AvlTreeNode<TK, TV>(key, value);
            Root = Root == null ? node : InsertOrUpdateRecursive(Root, node);
        }

        private static AvlTreeNode<TK, TV> InsertOrUpdateRecursive(AvlTreeNode<TK, TV> currentNode,
            AvlTreeNode<TK, TV> nodeToInsert)
        {
            if (currentNode == null)
            {
                currentNode = nodeToInsert;
                return currentNode;
            }

            if (nodeToInsert.Key.CompareTo(currentNode.Key) < 0)
            {
                currentNode.LeftNode = InsertOrUpdateRecursive(currentNode.LeftNode, nodeToInsert);
                currentNode = BalanceSubtree(currentNode);
            }
            else if (nodeToInsert.Key.CompareTo(currentNode.Key) > 0)
            {
                currentNode.RightNode = InsertOrUpdateRecursive(currentNode.RightNode, nodeToInsert);
                currentNode = BalanceSubtree(currentNode);
            }

            return currentNode;
        }

        public void Delete(TK key)
        {
            Root = DeleteRecursive(Root, key);
        }

        private static AvlTreeNode<TK, TV> DeleteRecursive(AvlTreeNode<TK, TV> currentNode, TK key)
        {
            if (currentNode == null) return null;

            if (key.CompareTo(currentNode.Key) < 0)
            {
                currentNode.LeftNode = DeleteRecursive(currentNode.LeftNode, key);
                currentNode = BalanceSubtree(currentNode);
            }

            else if (key.CompareTo(currentNode.Key) > 0)
            {
                currentNode.RightNode = DeleteRecursive(currentNode.RightNode, key);
                currentNode = BalanceSubtree(currentNode);
            }
            else
            {
                // если есть правая дочерняя нода
                if (currentNode.RightNode != null)
                {
                    // находим последователя ноды
                    var parent = currentNode.RightNode;
                    while (parent.LeftNode != null) parent = parent.LeftNode;
                    currentNode.Key = parent.Key;
                    currentNode.RightNode = DeleteRecursive(currentNode.RightNode, parent.Key);
                    currentNode = BalanceSubtree(currentNode);
                }
                else
                {
                    return currentNode.LeftNode;
                }
            }

            return currentNode;
        }


        /// <summary>
        ///     Метод выполняет балансировку дерева с помощью осуществления поворотов. Каждый из поворотов
        ///     возвращает вершину, ставшую корнем сбалансированного поддерева
        /// </summary>
        /// <param name="currentNode">Корневая нода балансируемого поддерева</param>
        private static AvlTreeNode<TK, TV> BalanceSubtree(AvlTreeNode<TK, TV> currentNode)
        {
            var subtreesHeightDifference = GetSubtreesHeightDifference(currentNode);
            if (subtreesHeightDifference > 1)
            {
                if (GetSubtreesHeightDifference(currentNode.LeftNode) > 0)
                    currentNode = SmallRightRotation(currentNode);
                else
                    currentNode = BigRightRotation(currentNode);
            }
            else if (subtreesHeightDifference < -1)
            {
                if (GetSubtreesHeightDifference(currentNode.RightNode) > 0)
                    currentNode = BigLeftRotation(currentNode);
                else
                    currentNode = SmallLeftRotation(currentNode);
            }

            return currentNode;
        }

        private static int GetSubtreesHeightDifference(AvlTreeNode<TK, TV> currentNode)
        {
            return GetSubtreeHeight(currentNode.LeftNode) - GetSubtreeHeight(currentNode.RightNode);
        }

        private static int GetSubtreeHeight(AvlTreeNode<TK, TV> currentNode)
        {
            if (currentNode == null) return 0;
            return Math.Max(GetSubtreeHeight(currentNode.LeftNode), GetSubtreeHeight(currentNode.RightNode)) + 1;
        }

        /// <summary>
        ///     Малое левое вращение
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        private static AvlTreeNode<TK, TV> SmallLeftRotation(AvlTreeNode<TK, TV> root)
        {
            var pivot = root.RightNode;
            root.RightNode = pivot.LeftNode;
            pivot.LeftNode = root;
            return pivot;
        }

        /// <summary>
        ///     Малое правое вращение
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        private static AvlTreeNode<TK, TV> SmallRightRotation(AvlTreeNode<TK, TV> root)
        {
            var pivot = root.LeftNode;
            root.LeftNode = pivot.RightNode;
            pivot.RightNode = root;
            return pivot;
        }

        /// <summary>
        ///     Большое правое вращение
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        private static AvlTreeNode<TK, TV> BigRightRotation(AvlTreeNode<TK, TV> root)
        {
            var pivot = root.LeftNode;
            root.LeftNode = SmallLeftRotation(pivot);
            return SmallRightRotation(root);
        }

        /// <summary>
        ///     Большое левое вращение
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        private static AvlTreeNode<TK, TV> BigLeftRotation(AvlTreeNode<TK, TV> root)
        {
            var pivot = root.RightNode;
            root.RightNode = SmallRightRotation(pivot);
            return SmallLeftRotation(root);
        }

        public IList<AvlTreeNode<TK, TV>> InOrderTraversal()
        {
            var nodes = new List<AvlTreeNode<TK, TV>>();
            if (Root == null) return nodes;
            InOrderTraversalRecursive(Root, nodes);
            return nodes;
        }

        public IList<AvlTreeNode<TK, TV>> InOrderTraversalByLevels()
        {
            var nodes = new List<AvlTreeNode<TK, TV>>();

            for (var i = 1; i <= GetSubtreeHeight(Root); i++) InOrderTraversalCurrentLevel(Root, i, nodes);

            return nodes;
        }

        private static void InOrderTraversalCurrentLevel(AvlTreeNode<TK, TV> root, int level,
            IList<AvlTreeNode<TK, TV>> nodes)
        {
            if (root == null) return;
            if (level == 1)
            {
                nodes.Add(root);
            }
            else if (level > 1)
            {
                InOrderTraversalCurrentLevel(root.LeftNode, level - 1, nodes);
                InOrderTraversalCurrentLevel(root.RightNode, level - 1, nodes);
            }
        }

        private static void InOrderTraversalRecursive(AvlTreeNode<TK, TV> currentNode, IList<AvlTreeNode<TK, TV>> nodes)
        {
            if (currentNode == null) return;
            InOrderTraversalRecursive(currentNode.LeftNode, nodes);
            nodes.Add(currentNode);
            InOrderTraversalRecursive(currentNode.RightNode, nodes);
        }
    }
}