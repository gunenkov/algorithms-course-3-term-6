﻿namespace Lab6.Models
{
    public class HuffmanTreeNode
    {
        public HuffmanTreeNode(char character, int value)
        {
            Character = character;
            Value = value;
        }

        public HuffmanTreeNode(char? character, int value, HuffmanTreeNode leftNode, HuffmanTreeNode rightNode)
        {
            Character = character;
            Value = value;
            LeftNode = leftNode;
            RightNode = rightNode;
        }

        public char? Character { get; set; }

        public int Value { get; set; }

        public HuffmanTreeNode LeftNode { get; set; }
        public HuffmanTreeNode RightNode { get; set; }

        public string GetCharacterCode(char? character, string path)
        {
            if (Character == character) return path;

            if (LeftNode != null)
            {
                var updatedPath = LeftNode.GetCharacterCode(character, path + '0');
                if (updatedPath != null) return updatedPath;
            }

            if (RightNode != null)
            {
                var updatedPath = RightNode.GetCharacterCode(character, path + '1');
                if (updatedPath != null) return updatedPath;
            }

            return null;
        }
    }
}