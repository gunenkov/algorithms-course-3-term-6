﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab6.Models;

namespace Lab6
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var text = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(text)) return;

            var counts = GetCharacterCounts(text);
            var nodes = counts.Keys.Select(character => new HuffmanTreeNode(character, counts[character])).ToList();
            var huffmanTree = BuildHuffmanTree(nodes);

            var codes = counts.Keys.ToDictionary(character => character,
                character => huffmanTree.GetCharacterCode(character, string.Empty));

            Console.WriteLine($"Исходное сообщение: \"{text}\". Объем исходного сообщения: {text.Length * 8}");

            var encoded = HuffmanEncode(text, codes);
            Console.WriteLine(
                $"Закодированное сообщение: \"{encoded}\". Объем закодированного сообщения: {encoded.Length}");
            var decoded = HuffmanDecode(encoded, huffmanTree);
            Console.WriteLine(
                $"Декодированное сообщение: \"{decoded}\". Объем декодированного сообщения: {decoded.Length * 8}");
        }

        private static string HuffmanEncode(string text, IDictionary<char, string> codes)
        {
            var codedSb = new StringBuilder();
            foreach (var character in text) codedSb.Append(codes[character]);
            return codedSb.ToString();
        }

        private static string HuffmanDecode(string code, HuffmanTreeNode huffmanTree)
        {
            var decodeSb = new StringBuilder();

            var currentNode = huffmanTree;
            foreach (var symbol in code)
            {
                currentNode = symbol == '0' ? currentNode.LeftNode : currentNode.RightNode;
                if (currentNode.Character == null) continue;

                decodeSb.Append(currentNode.Character);
                currentNode = huffmanTree;
            }

            return decodeSb.ToString();
        }

        /// <summary>
        ///     Получаем словарь, ключи в котором - символы переданного текста; значения - частота
        ///     каждого символа в тексте
        /// </summary>
        /// <param name="text">Текст, для которого составляется словарь</param>
        /// <returns>Словарь, характеризующий частоту встречаемости символов в тексте</returns>
        private static Dictionary<char, int> GetCharacterCounts(string text)
        {
            var counts = new Dictionary<char, int>();
            foreach (var character in text)
                if (counts.ContainsKey(character))
                    counts[character]++;
                else
                    counts[character] = 1;

            return counts;
        }

        private static HuffmanTreeNode BuildHuffmanTree(IList<HuffmanTreeNode> nodes)
        {
            while (nodes.Count > 1)
            {
                nodes = nodes.OrderByDescending(node => node.Value).ToList();
                var leftNode = nodes[^1];
                var rightNode = nodes[^2];
                nodes.Remove(leftNode);
                nodes.Remove(rightNode);
                nodes.Add(new HuffmanTreeNode(null, leftNode.Value + rightNode.Value, leftNode, rightNode));
            }

            return nodes.First();
        }
    }
}