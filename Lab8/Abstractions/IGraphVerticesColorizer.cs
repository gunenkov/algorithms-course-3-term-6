﻿using System.Collections.Generic;
using Lab8.Models;

namespace Lab8.Abstractions
{
    internal interface IGraphVerticesColorizer
    {
        /// <summary>
        ///     Метод выполняет вершинную раскраску графа (изменяется параметр)
        /// </summary>
        /// <param name="graph">
        ///     Граф, представленный в виде словаря, ключами в котором
        ///     являются вершины, а значением - список смежных вершин для рассматриваемой вершины
        /// </param>
        void ColorizeVerices(IDictionary<Vertex, IList<Vertex>> graph);
    }
}