﻿using System.Collections.Generic;
using Lab8.Abstractions;
using Lab8.Models;

namespace Lab8
{
    public class GreedyGraphVerticesColorizer : IGraphVerticesColorizer
    {
        public IDictionary<int, IList<Vertex>> ColorGroups { get; } = new Dictionary<int, IList<Vertex>>();

        public void ColorizeVerices(IDictionary<Vertex, IList<Vertex>> graph)
        {
            ColorGroups.Clear();
            var currentColor = 1;
            ColorGroups.Add(currentColor, new List<Vertex>());

            foreach (var (vertex, adjacentVertices) in graph)
            {
                foreach (var adjacentVertex in adjacentVertices)
                    // если какая-либо смежная вершина раскрашена в текущий цвет - необходимо выбрать новый цвет
                    if (ColorGroups[currentColor].Contains(adjacentVertex))
                    {
                        currentColor++;
                        break;
                    }

                vertex.Color = currentColor;

                if (!ColorGroups.ContainsKey(currentColor)) ColorGroups.Add(currentColor, new List<Vertex>());

                ColorGroups[currentColor].Add(vertex);
            }
        }
    }
}