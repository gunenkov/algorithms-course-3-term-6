﻿namespace Lab8.Models
{
    public class Vertex
    {
        public int Id { get; set; }
        public int? Color { get; set; }

        protected bool Equals(Vertex other)
        {
            return Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Vertex) obj);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override string ToString()
        {
            return $"Метка: {Id}; Цвет: {Color}";
        }
    }
}