﻿using System;
using System.Collections.Generic;
using Lab8.Abstractions;
using Lab8.Models;

namespace Lab8
{
    internal class Program
    {
        private static void Main()
        {
            // ... подготовка данных ...

            // инициализация графа
            var graph = new Dictionary<Vertex, IList<Vertex>>();

            // инициализация списка вершин
            var vertices = new Vertex[4];
            for (var i = 0; i < 4; i++) vertices[i] = new Vertex {Id = i + 1};

            // построение структуры графа
            graph[vertices[0]] = new List<Vertex>
            {
                vertices[1], vertices[2]
            };

            graph[vertices[1]] = new List<Vertex>
            {
                vertices[0]
            };

            graph[vertices[2]] = new List<Vertex>
            {
                vertices[0]
            };

            graph[vertices[3]] = new List<Vertex>
            {
                vertices[2]
            };

            // работа с объектом, выполняющим раскраску вершин графа
            IGraphVerticesColorizer colorizer = new GreedyGraphVerticesColorizer();

            colorizer.ColorizeVerices(graph);

            foreach (var vertex in vertices) Console.WriteLine(vertex);
        }
    }
}