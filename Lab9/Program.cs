﻿using System;
using System.Collections.Generic;

namespace Lab9
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            #region Задача 1

            var task1Result = Task1(new List<Point>
            {
                new Point
                {
                    Name = "1",
                    Distance = 6
                },
                new Point
                {
                    Name = "2",
                    Distance = 8
                },
                new Point
                {
                    Name = "3",
                    Distance = 16
                },
                new Point
                {
                    Name = "4",
                    Distance = 20
                }
            }, 24, 9, 1);

            Console.WriteLine("Задача 1");
            foreach (var point in task1Result) Console.WriteLine(point);

            #endregion

            Console.WriteLine();

            #region Задача 2

            var task2Result = Task2(new double[] {12, 14, 8}, new[,]
            {
                {6.0, 3, 4},
                {2, 1, 2},
                {10, 3, 3}
            });

            Console.WriteLine("Задача 2");
            foreach (var index in task2Result) Console.WriteLine(index);

            #endregion
        }

        public static IList<Point> Task1(IList<Point> points, float pathLenght, float dayDuration, float speed)
        {
            points.Add(new Point {Name = "Stop", Distance = pathLenght});

            var stopPoints = new List<Point>();
            // сколько прошли к текущему моменту
            var currentDistance = 0.0;
            // сколько могут пройти до следующей остановки
            var resource = speed * dayDuration;

            while (currentDistance < pathLenght)
            {
                var optPoint = new Point {Distance = 0};
                foreach (var point in points)
                    if (point.Distance - currentDistance > optPoint.Distance - currentDistance &&
                        point.Distance - currentDistance < resource)
                        optPoint = point;
                stopPoints.Add(optPoint);
                var lastDistance = optPoint.Distance - currentDistance;
                currentDistance += lastDistance;
            }

            return stopPoints;
        }

        /// <summary>
        ///     Метод решает задачу 2 лабораторной работы 9
        /// </summary>
        /// <param name="initialCosts">Начальные стоимости оборудования</param>
        /// <param name="incByMonths">
        ///     Матрица, по строкам которой расположены виды оборудования,
        ///     по столбцам - месяца. Элемент матрицы - увеличение стоимости единицы оборудования
        ///     в текущем месяце
        /// </param>
        /// <returns>Последовательность закупки оборудования</returns>
        public static IList<double> Task2(IList<double> initialCosts, double[,] incByMonths)
        {
            var sequenceOfBuying = new List<double>();
            var currentCosts = initialCosts;

            for (var monthNumber = 0; monthNumber < incByMonths.GetLength(1); monthNumber++)
            {
                var maximumCost = currentCosts[0] = currentCosts[0] + incByMonths[0, monthNumber];
                var optProductIndex = 0;
                for (var productIndex = 1; productIndex < incByMonths.GetLength(0); productIndex++)
                {
                    currentCosts[productIndex] = currentCosts[productIndex] + incByMonths[productIndex, monthNumber];
                    if (maximumCost >= currentCosts[productIndex]) continue;
                    maximumCost = currentCosts[productIndex];
                    optProductIndex = productIndex;
                }

                sequenceOfBuying.Add(optProductIndex);
                initialCosts[optProductIndex] = double.MinValue;
            }

            return sequenceOfBuying;
        }

        public class Point
        {
            public string Name { get; set; }

            public float Distance { get; set; }

            public override string ToString()
            {
                return $"Название пункта: {Name}. Расстояние от начала: {Distance}";
            }
        }
    }
}