﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab11
{
    internal class Program
    {
        private static void Main()
        {
            var shoppingList = new List<Item>
            {
                new Item("Ручка", 200, 4),
                new Item("Тетраль", 350, 6),
                new Item("Пенал", 600, 1)
            };

            PrintAllPurchaseOptions(shoppingList, 1150);
        }

        public static void PrintAllPurchaseOptions(IList<Item> shoppingList, int amountOfMoney)
        {
            var zeroBinaryVector = Enumerable.Repeat(0, shoppingList.Count).ToArray();
            var binaryVector = (int[]) zeroBinaryVector.Clone();

            // необходимо генерировать бинарные векторы для формирования списка наименований

            var bestPurchaseOption = new Dictionary<string, int>();
            var difference = amountOfMoney;

            for (var i = 0; i < Math.Pow(2, shoppingList.Count) - 1; i++)
            {
                var rightZeroElementIndex = -1;
                for (var j = shoppingList.Count - 1; j >= 0; j--)
                {
                    if (binaryVector[j] != 0) continue;

                    binaryVector[j] = 1;
                    rightZeroElementIndex = j;
                    break;
                }

                for (var k = rightZeroElementIndex + 1; k < shoppingList.Count; k++) binaryVector[k] = 0;

                // бинарный вектор сгенерирован, начинаем формировать текущий вариант покупки

                var purchaseOption = new Dictionary<string, int>();
                var costOfPurchaseOption = 0;
                var purchaseAvaibilityBinaryVector = (int[]) binaryVector.Clone();

                /*
                 * Цель закупки - получения наибольшего количества наименований товаров, при
                 * удовлетворении ограничений на стоимость и желаемое количество единиц данного
                 * товара. Введем бинарный вектор purchaseAvaibilityBinaryVector, размер которого
                 * совпадает с размером списка покупок. При несоблюдении одного из ограничений
                 * задачи для данного товара значение соответствующей компоненты вектора будет 0,
                 * в связи с чем данный товар не будет накапливаться в корзине покупателя.
                 *
                 * Будем считать, что текущий вариант закупки сформирован, как только стоимость
                 * корзины превысила стоимость имеющихся денег или как только вектор purchaseAvaibilityBinaryVector
                 * совпал с нулевым вектором.
                 *
                 * Сам процесс формирования корзины представляет собой закольцованный перебор
                 * предметов из списка закупок, которым соответствуют значения 1 вектора
                 * purchaseAvaibilityBinaryVector
                 */

                while (costOfPurchaseOption < amountOfMoney &&
                       !purchaseAvaibilityBinaryVector.SequenceEqual(zeroBinaryVector))
                    for (var j = 0; j < shoppingList.Count; j++)
                    {
                        if (purchaseAvaibilityBinaryVector[j] != 1) continue;

                        // если денег хватит на добавление в корзину единицы данного товара
                        if (shoppingList[j].Cost + costOfPurchaseOption <= amountOfMoney)
                        {
                            // добавляем единицу товара в корзину
                            if (purchaseOption.ContainsKey(shoppingList[j].Name))
                                purchaseOption[shoppingList[j].Name]++;
                            else
                                purchaseOption.Add(shoppingList[j].Name, 1);

                            /*
                             * Если в корзине присутствует необходимое количество единиц данного
                             * товара, значит данный товар можно далее не рассматривать
                             */

                            if (purchaseOption[shoppingList[j].Name] >= shoppingList[j].NeededCount)
                                purchaseAvaibilityBinaryVector[j] = 0;

                            // обновляем стоимость корзины
                            costOfPurchaseOption += shoppingList[j].Cost;
                        }
                        /*
                         * Если не хватит денег на добавление единицы данного товара в корзину,
                         * значит данный товар можно далее не рассматривать
                         */
                        else
                        {
                            purchaseAvaibilityBinaryVector[j] = 0;
                        }
                    }

                var currentDifference = amountOfMoney - costOfPurchaseOption;

                if (difference <= currentDifference) continue;
                bestPurchaseOption = purchaseOption;
                difference = currentDifference;
            }

            // выводим на экран лучшую корзину

            Console.WriteLine("Лучший выбор:\n");
            foreach (var (itemName, countOfCurrentItem) in bestPurchaseOption)
                Console.WriteLine($"{itemName}: {countOfCurrentItem}");
            Console.WriteLine($"\nСтоимость корзины: {amountOfMoney - difference}");
        }

        public class Item
        {
            public Item(string name, int cost, int neededCount)
            {
                Name = name;
                Cost = cost;
                NeededCount = neededCount;
            }

            public string Name { get; set; }
            public int Cost { get; set; }
            public int NeededCount { get; set; }
        }
    }
}