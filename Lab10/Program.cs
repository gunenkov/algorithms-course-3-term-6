﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab10
{
    internal class Program
    {
        private static void Main()
        {
            Console.WriteLine("Задание 1");
            PrintAllPermutations(new[] {1, 2, 3, 4});
            Console.WriteLine();
            Console.WriteLine("Задание 2");
            PrintAllSubsets(new object[] {"стол", "стул", "шкаф", "кресло"});
        }

        /// <summary>
        ///     Метод выводит на экран все перестановки по алгоритму Джонсона-Троттера
        /// </summary>
        /// <param name="elements">Множество элементов для генерации перестановок</param>
        public static void PrintAllPermutations(IList<int> elements)
        {
            var orderedElements = elements.OrderBy(e => e).ToList();
            var directions = Enumerable.Repeat(-1, elements.Count).ToList();

            while (true)
            {
                // выводим на экран текущую перестановку

                Console.WriteLine(string.Join(", ", orderedElements) + " " +
                                  string.Join(", ", directions.Select(e => e.ToString()
                                      .Replace("-1", "<-").Replace("1", "->"))));

                // находим наибольший мобильный элемент

                var maxMobileElementIndex = -1;
                var maxMobileElement = 0;

                for (var i = 0; i < orderedElements.Count; i++)
                {
                    if (directions[i] < 0 && i > 0)
                        if (orderedElements[i] > orderedElements[i - 1] && orderedElements[i] > maxMobileElement)
                        {
                            maxMobileElementIndex = i;
                            maxMobileElement = orderedElements[i];
                        }

                    if (directions[i] > 0 && i < orderedElements.Count() - 1)
                        if (orderedElements[i] > orderedElements[i + 1] && orderedElements[i] > maxMobileElement)
                        {
                            maxMobileElementIndex = i;
                            maxMobileElement = orderedElements[i];
                        }
                }

                // если не нашли мобильного элемента - все перестановки выписаны

                if (maxMobileElementIndex < 0) break;

                // меняем местами мобильный элемент с соседом, по направлению стрелки

                if (directions[maxMobileElementIndex] < 0)
                {
                    var tempElement = orderedElements[maxMobileElementIndex];
                    orderedElements[maxMobileElementIndex] = orderedElements[maxMobileElementIndex - 1];
                    orderedElements[maxMobileElementIndex - 1] = tempElement;

                    var tempDirection = directions[maxMobileElementIndex];
                    directions[maxMobileElementIndex] = directions[maxMobileElementIndex - 1];
                    directions[maxMobileElementIndex - 1] = tempDirection;
                }

                else
                {
                    var temp = orderedElements[maxMobileElementIndex];
                    orderedElements[maxMobileElementIndex] = orderedElements[maxMobileElementIndex + 1];
                    orderedElements[maxMobileElementIndex + 1] = temp;

                    var tempDirection = directions[maxMobileElementIndex];
                    directions[maxMobileElementIndex] = directions[maxMobileElementIndex + 1];
                    directions[maxMobileElementIndex + 1] = tempDirection;
                }

                // меняем стрелки у всех элементов, которые больше найденного

                for (var i = 0; i < orderedElements.Count; i++)
                    if (orderedElements[i] > maxMobileElement)
                    {
                        if (directions[i] < 0)
                            directions[i] = 1;
                        else
                            directions[i] = -1;
                    }
            }
        }

        /// <summary>
        ///     Метод выводит на экран все возможные варианты выборки из данных элементов
        /// </summary>
        /// <param name="elements">Список элементов, из которых необходимо формировать подмножества</param>
        public static void PrintAllSubsets(IList<object> elements)
        {
            var binaryVector = Enumerable.Repeat(0, elements.Count).ToArray();

            for (var i = 0; i < Math.Pow(2, elements.Count) - 1; i++)
            {
                // сгенерируем следующий бинарный вектор

                var rightZeroElementIndex = -1;
                for (var j = elements.Count - 1; j >= 0; j--)
                {
                    if (binaryVector[j] != 0) continue;

                    binaryVector[j] = 1;
                    rightZeroElementIndex = j;
                    break;
                }

                for (var k = rightZeroElementIndex + 1; k < elements.Count; k++) binaryVector[k] = 0;

                // бинарный вектор сгенерирован, сформируем на его основе подмножество

                var subset = new List<object>();
                for (var j = 0; j < elements.Count; j++)
                    if (binaryVector[j] > 0)
                        subset.Add(elements[j]);

                // выведем подмножество на экран

                Console.WriteLine(string.Join(", ", subset));
            }
        }
    }
}