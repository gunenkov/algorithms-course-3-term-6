﻿using System;
using TreeAlgorithms.AVL;

namespace Lab4
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var avlTree = new AvlTree<int, int>();
            avlTree.InsertOrUpdate(4, 6);
            avlTree.InsertOrUpdate(7, 6);
            avlTree.InsertOrUpdate(5, 6);
            avlTree.InsertOrUpdate(6, 6);

            foreach (var node in avlTree.InOrderTraversalByLevels())
                Console.WriteLine($"Key: {node.Key}\tValue: {node.Value}");
        }
    }
}