﻿using System;
using TreeAlgorithms.RedBlack;

namespace Lab5
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var redBlackTree = new RedBlackTree<int, object>();
            redBlackTree.Insert(8, null);
            redBlackTree.Insert(4, null);
            redBlackTree.Insert(9, null);
            redBlackTree.Insert(3, null);
            //redBlackTree.Delete(3);
            redBlackTree.Insert(2, null);
            redBlackTree.Insert(1, null);
            foreach (var node in redBlackTree.InOrderTraversalByLevels())
                Console.WriteLine($"Key: {node.Key}\tValue: {node.Value}\tColor:{node.Color}");
        }
    }
}