﻿namespace Lab12.Models
{
    public class ClientInfo
    {
        public string FullName { get; set; }
        public int CallsLength { get; set; }
        public int CostOfOneMinute { get; set; } = 2;
        public int Cost => CallsLength * CostOfOneMinute;

        public override string ToString()
        {
            return $"{FullName} Минуты: {CallsLength} К оплате: {Cost}";
        }
    }
}