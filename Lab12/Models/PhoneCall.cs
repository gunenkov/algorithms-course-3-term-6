﻿namespace Lab12.Models
{
    public class PhoneCall
    {
        public string PhoneNumber { get; set; }
        public string FullName { get; set; }
        public int CallLength { get; set; }
    }
}