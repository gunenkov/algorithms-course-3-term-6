﻿using System;
using System.Threading.Tasks;
using Lab12.Models;
using Lab12.Services.Client;
using Lab12.Services.Client.Abstractions;
using Lab12.Services.Cryptography;
using Lab12.Services.Mock;
using Lab12.Services.Mock.Abstractions;

namespace Lab12
{
    internal class Program
    {
        private static async Task Main()
        {
            IPhoneCallsMockService mock = new PhoneCallsMockService();
            ISummaryTableService summaryTableService = new SummaryTableService();

            const string fileName = "calls.txt";

            // создаем mock-файл
            await mock.InitMockFile(fileName);

            // считываем файл, инициализируем таблицу
            await summaryTableService.InitSummaryTable(fileName);

            // выводим таблицу на печать
            summaryTableService.PrintSummaryTable();

            Console.WriteLine();

            // удаляем разговор
            summaryTableService.DeletePhoneCallsByNumber("9135555333");

            // выводим таблицу на печать
            summaryTableService.PrintSummaryTable();

            Console.WriteLine();

            // добавляем разговор (клиент существует)
            summaryTableService.AddPhoneCall(new PhoneCall
            {
                PhoneNumber = "9135555555",
                CallLength = 3
            });

            // выводим таблицу на печать
            summaryTableService.PrintSummaryTable();

            Console.WriteLine();

            // Md5
            Console.WriteLine("MD5 for: \"Mikhail\"");
            var md5Hash = new Md5().ComputeHash("Mikhail");
            Console.WriteLine($"{md5Hash} Длина: {md5Hash.Length}");

            Console.WriteLine();

            // Sha1
            Console.WriteLine("SHA-1 for: \"Mikhail\"");
            var sha1Hash = new Sha1().ComputeHash("Mikhail");
            Console.WriteLine($"{sha1Hash} Длина: {sha1Hash.Length}");
        }
    }
}