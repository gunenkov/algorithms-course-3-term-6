﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Lab12.Models;

namespace Lab12.Providers.Abstractions
{
    public interface IPhoneCallsProvider
    {
        Task<IList<PhoneCall>> Get(string source);
    }
}