﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Lab12.Models;
using Lab12.Providers.Abstractions;

namespace Lab12.Providers
{
    public class PhoneCallsFromFileProvider : IPhoneCallsProvider
    {
        public async Task<IList<PhoneCall>> Get(string path)
        {
            var lines = await File.ReadAllLinesAsync(path);
            var phoneCalls = new List<PhoneCall>();

            foreach (var line in lines)
            {
                var parts = line.Split(new[] {';'});
                var phoneCall = new PhoneCall
                {
                    PhoneNumber = parts[0],
                    FullName = parts[1],
                    CallLength = Convert.ToInt32(parts[2])
                };
                phoneCalls.Add(phoneCall);
            }

            return phoneCalls;
        }
    }
}