﻿using System.Threading.Tasks;
using Lab12.Models;

namespace Lab12.Services.Client.Abstractions
{
    public interface ISummaryTableService
    {
        void PrintSummaryTable();
        Task InitSummaryTable(string source);
        void AddPhoneCall(PhoneCall phoneCall);
        void DeletePhoneCallsByNumber(string phoneNumber);
    }
}