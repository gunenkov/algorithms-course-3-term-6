﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lab12.Models;
using Lab12.Providers;
using Lab12.Providers.Abstractions;
using Lab12.Services.Client.Abstractions;
using Lab12.Services.Cryptography;
using Lab12.Services.Cryptography.Abstractions;

namespace Lab12.Services.Client
{
    public class SummaryTableService : ISummaryTableService
    {
        private readonly NumericHash _hash = new PolynomicalHash();
        private readonly IDictionary<long, ClientInfo> _summaryTable = new Dictionary<long, ClientInfo>();
        private IList<PhoneCall> _phoneCalls;

        public void PrintSummaryTable()
        {
            foreach (var (numberHash, clientInfo) in _summaryTable) Console.WriteLine($"{numberHash}: {clientInfo}");
        }

        public async Task InitSummaryTable(string source)
        {
            IPhoneCallsProvider phoneCallsProvider = new PhoneCallsFromFileProvider();
            _phoneCalls = await phoneCallsProvider.Get(source);

            foreach (var phoneCall in _phoneCalls)
            {
                var numberHash = _hash.ComputeHash(phoneCall.PhoneNumber);
                InsertOrUpdateSummaryTableRecord(numberHash, phoneCall);
            }
        }

        public void AddPhoneCall(PhoneCall phoneCall)
        {
            var phoneNumber = phoneCall.PhoneNumber;
            var numberHash = _hash.ComputeHash(phoneNumber);

            _phoneCalls.Add(phoneCall);
            InsertOrUpdateSummaryTableRecord(numberHash, phoneCall);
        }

        public void DeletePhoneCallsByNumber(string phoneNumber)
        {
            _phoneCalls = _phoneCalls.Where(c => c.PhoneNumber != phoneNumber).ToList();
            var numberHash = _hash.ComputeHash(phoneNumber);
            _summaryTable.Remove(numberHash);
        }

        private void InsertOrUpdateSummaryTableRecord(long numberHash, PhoneCall phoneCall)
        {
            if (_summaryTable.ContainsKey(numberHash))
                _summaryTable[numberHash].CallsLength += phoneCall.CallLength;
            else
                _summaryTable.Add(numberHash, new ClientInfo
                {
                    CallsLength = phoneCall.CallLength,
                    FullName = phoneCall.FullName
                });
        }
    }
}