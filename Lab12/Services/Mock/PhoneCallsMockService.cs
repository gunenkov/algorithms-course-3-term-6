﻿using System.IO;
using System.Threading.Tasks;
using Lab12.Services.Mock.Abstractions;

namespace Lab12.Services.Mock
{
    public class PhoneCallsMockService : IPhoneCallsMockService
    {
        public async Task InitMockFile(string path)
        {
            var lines = new[]
            {
                "9135555555;Гуненков;4",
                "9135555333;Дюсекенов;4",
                "9135555555;Гуненков;2"
            };

            await File.WriteAllLinesAsync(path, lines);
        }
    }
}