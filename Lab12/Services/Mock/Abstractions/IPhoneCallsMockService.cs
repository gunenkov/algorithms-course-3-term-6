﻿using System.Threading.Tasks;

namespace Lab12.Services.Mock.Abstractions
{
    public interface IPhoneCallsMockService
    {
        Task InitMockFile(string path);
    }
}