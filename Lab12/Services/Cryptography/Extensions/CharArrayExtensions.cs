﻿using System;

namespace Lab12.Services.Cryptography.Extensions
{
    public static class CharArrayExtensions
    {
        public static uint ToInt64(this char[] array)
        {
            return (uint) Convert.ToInt64(new string(array), 2);
        }
    }
}