﻿namespace Lab12.Services.Cryptography.Abstractions
{
    public abstract class StringHash : IHash<string>
    {
        public abstract string ComputeHash(string text);
    }
}