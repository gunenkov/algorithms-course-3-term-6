﻿namespace Lab12.Services.Cryptography.Abstractions
{
    public interface IHash<T>
    {
        T ComputeHash(string text);
    }
}