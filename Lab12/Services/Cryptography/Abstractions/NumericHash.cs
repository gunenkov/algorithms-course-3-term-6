﻿namespace Lab12.Services.Cryptography.Abstractions
{
    public abstract class NumericHash : IHash<long>
    {
        public abstract long ComputeHash(string text);
    }
}