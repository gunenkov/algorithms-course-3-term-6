﻿using System;
using System.Linq;
using Lab12.Services.Cryptography.Abstractions;

namespace Lab12.Services.Cryptography
{
    public class PolynomicalHash : NumericHash
    {
        public int K { get; set; } = 17;
        public int P { get; set; } = 1046527;

        public override long ComputeHash(string text)
        {
            var polynomSum = text.Select((character, n) => Convert.ToInt32(character) * (long) Math.Pow(K, n)).Sum();
            return polynomSum % P;
        }
    }
}