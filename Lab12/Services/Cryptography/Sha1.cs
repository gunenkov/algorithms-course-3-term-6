﻿using System;
using System.Linq;
using Lab12.Services.Cryptography.Abstractions;
using Lab12.Services.Cryptography.Extensions;

namespace Lab12.Services.Cryptography
{
    public class Sha1 : StringHash
    {
        private static uint BitwiseLeftRotate(uint a, int offset)
        {
            return (a << offset) | (a >> (32 - offset));
        }

        public override string ComputeHash(string text)
        {
            // инициализация переменных, из которых сложится хэш
            uint h0 = 0x67452301;
            uint h1 = 0xEFCDAB89;
            uint h2 = 0x98BADCFE;
            uint h3 = 0x10325476;
            uint h4 = 0xC3D2E1F0;

            // делим строку на отдельные символы, получаем ASCI коды
            // переводим их в двоичный формат, по 8 битов
            var characters = text
                .Select(c => (int) c)
                .Select(c => Convert.ToString(c, 2)
                    .PadLeft(8, '0'))
                .ToArray();

            // объединяем полученные двоичные значения и добавляем справа '1'

            var binaryString = string.Join("", characters) + "1";
            var length = binaryString.Length - 1;

            // пока остаток от деления длины строки на 512 не равен 448 добавляем справа нули

            while (binaryString.Length % 512 != 448) binaryString += "0";

            /*
             * переводим длину объединенного массива двоичных значений (без 1 в конце)
             * в двоичную систему. Заполняем нулями справа, пока не получим 64 бита
             */
            var lengthBinary = Convert.ToString(length, 2).PadLeft(64, '0');

            // объединяем полученные строки
            binaryString += lengthBinary;

            // разделяем полученную строку на чанки по 512 бит и каждый чанк - на слова по 32 бита
            var chunks = binaryString
                .Select((bit, i) => new {Value = bit, Index = i})
                .GroupBy(x => x.Index / 512)
                .Select(chunk => chunk.Select(x => x.Value).ToArray())
                .ToArray();

            //После того, как разбили на чанки по 512 бит - перебираем чанки
            foreach (var chunk in chunks)
            {
                // разбиваем чанк на 16 слов по 32 бита
                var words = chunk
                    .Select((bit, i) => new {Value = bit, Index = i})
                    .GroupBy(x => x.Index / 32)
                    .Select(word => word.Select(x => x.Value).ToArray().ToInt64())
                    .ToList();

                // 16 слов по 32-бита дополняются до 80 32-битовых слов
                for (var i = 16; i <= 79; i++)
                    words.Add(BitwiseLeftRotate(words[i - 3] ^ words[i - 8] ^ words[i - 14] ^ words[i - 16], 1));

                // инициализация хэшей чанка
                var a = h0;
                var b = h1;
                var c = h2;
                var d = h3;
                var e = h4;

                // для каждого чанка проводим 80 раундов

                uint k = 0, f = 0;
                for (var round = 0; round < 80; round++)
                {
                    if (round >= 0 && round < 20)
                    {
                        f = (b & c) | (~b & d);
                        k = 0x5A827999;
                    }
                    else if (round >= 20 && round < 40)
                    {
                        f = b ^ c ^ d;
                        k = 0x6ED9EBA1;
                    }
                    else if (round >= 40 && round < 60)
                    {
                        f = (b & c) | (b & d) | (c & d);
                        k = 0x8F1BBCDC;
                    }
                    else if (round >= 60 && round < 80)
                    {
                        f = b ^ c ^ d;
                        k = 0xCA62C1D6;
                    }

                    var temp = BitwiseLeftRotate(a, 5) + f + e + k + words[round];
                    e = d;
                    d = c;
                    c = BitwiseLeftRotate(b, 30);
                    b = a;
                    a = temp;
                }

                // по окончанию раунда добавляем к общему хэшу хэш текущего чанка
                h0 += a;
                h1 += b;
                h2 += c;
                h3 += d;
                h4 += e;
            }

            // соединяем части общего хэша в строковую запись (160 бит = 40 байт = 40 символов)
            return h0.ToString("x8") + h1.ToString("x8") +
                   h2.ToString("x8") + h3.ToString("x8") + h4.ToString("x8");
        }
    }
}